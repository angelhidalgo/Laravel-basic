@extends('layouts.app')
   @php 
	   $category = [
	    	'0' => [
				'name' => 'White',
				'link' => '/categoria/White'
	    	],
	    	'1' => [
	    		'name' => 'Blue',
	    		'link' => '/categoria/Blue'
	    	],
	    	'2' => [
	    		'name' => 'Black',
	    		'link' => '/categoria/Black'
	    	],
	    	'3' => [
	    		'name' => 'Red',
	    		'link' => '/categoria/Red'
	    	],
	    	'4' => [
	    		'name' => 'Green',
	    		'link' => '/categoria/Green'
	    	],
	    	'5' => [
	    		'name' => 'Colorless',
	    		'link' => '/categoria/Colorless'
	    	]
	    ];
    @endphp

@section('content')
	<h1>about</h1>
	@foreach($category as $category)
		    <li class="nav-item">
		        {{ $category=>name }}
		    </li>
	@endforeach

@endsection

