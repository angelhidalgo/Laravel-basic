   @php 
	   ($category = [
	    	'0' => [
				'name' => 'White',
				'link' => '/categoria/White'
	    	],
	    	'1' => [
	    		'name' => 'Blue',
	    		'link' => '/categoria/Blue'
	    	],
	    	'2' => [
	    		'name' => 'Black',
	    		'link' => '/categoria/Black'
	    	],
	    	'3' => [
	    		'name' => 'Red',
	    		'link' => '/categoria/Red'
	    	],
	    	'4' => [
	    		'name' => 'Green',
	    		'link' => '/categoria/Green'
	    	],
	    	'5' => [
	    		'name' => 'Colorless',
	    		'link' => '/categoria/Colorless'
	    	]
	    ]);
    @endphp

@section('categories')
	@foreach($category as $category)
	    <li class="{{Request::is('{{!! $category[\'link\'] !!}}') ?  'nav-item  active' : 'nav-item '}}">
	        <a class="nav-link" href="/categoria/{{!! $category['link'] !!}}">{!!$category['name'] !!}</a>
	    </li>
	@endforeach